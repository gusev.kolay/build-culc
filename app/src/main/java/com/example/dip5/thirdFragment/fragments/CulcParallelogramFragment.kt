package com.example.dip5.thirdFragment.fragments

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.dip5.R
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.math.PI

class CulcParallelogramFragment: Fragment() {

    var size_A: Float? = null
    var size_B: Float? = null
    var size_C: Float? = null
    var number_sizeH: Float? = null
    var number_cost:  Int? = null


    var unit_for_first_size:  Float = 1.0F
    var unit_for_second_size: Float = 1.0F
    var unit_for_third_size: Float = 1.0F
    var unit_for_forth_size: Float = 1.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val list = listOf("м", "см", "мм")

        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_third_culc_parallelogram, container, false)

        //===============================================================================================================
        // actionBar configuration
        (activity as AppCompatActivity).supportActionBar?.title = "Вычисление площади прямоугольника"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //====================================================================================================================
        // initialisation basic components
        val spinner_sizeA = rootView.findViewById<Spinner>(R.id.spinner_sizeA)
        val spinner_sizeB = rootView.findViewById<Spinner>(R.id.spinner_sizeB)
        val spinner_sizeC = rootView.findViewById<Spinner>(R.id.spinner_sizeC)
        val spinner_sizeH = rootView.findViewById<Spinner>(R.id.spinner_sizeH)

        val spinner_adapter = ArrayAdapter(rootView.context, android.R.layout.simple_spinner_item, list)
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner_sizeA.adapter = spinner_adapter
        spinner_sizeB.adapter = spinner_adapter
        spinner_sizeC.adapter = spinner_adapter
        spinner_sizeH.adapter = spinner_adapter

        val field_sizeA = rootView.findViewById<EditText>(R.id.editText_sizeA)
        val field_sizeB = rootView.findViewById<EditText>(R.id.editText_sizeB)
        val field_sizeC = rootView.findViewById<EditText>(R.id.editText_sizeC)
        val field_sizeH = rootView.findViewById<EditText>(R.id.editText_sizeH)
        val field_cost = rootView.findViewById<EditText>(R.id.editText_cost)

        //======================================================================================================================
        //implementation listeners for Spinners
        spinner_sizeA.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        unit_for_first_size = 1.0F
                    }
                    1 -> {
                        unit_for_first_size = 0.01F
                    }
                    2 -> {
                        unit_for_first_size = 0.001F
                    }
                }
                calculate(rootView)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        spinner_sizeB.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        unit_for_second_size = 1.0F
                    }
                    1 -> {
                        unit_for_second_size = 0.01F
                    }
                    2 -> {
                        unit_for_second_size = 0.001F
                    }
                }
                calculate(rootView)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        spinner_sizeC.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position){
                    0 -> {
                        unit_for_third_size = 1.0F
                    }
                    1 -> {
                        unit_for_third_size = 0.01F
                    }
                    2 -> {
                        unit_for_third_size = 0.001F
                    }
                }
                calculate(rootView)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinner_sizeH.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_second_size = position
                when(position){
                    0 -> {
                        unit_for_forth_size = 1.0F
                    }
                    1 -> {
                        unit_for_forth_size = 0.01F
                    }
                    2 -> {
                        unit_for_forth_size = 0.001F
                    }
                }
                calculate(rootView)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        //=====================================================================================================
        // implementation listeners for fields
        field_sizeA.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                size_A = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }
                calculate(rootView)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sizeB.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                size_B = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }
                calculate(rootView)
            }

            override fun afterTextChanged(s: Editable?) {}
        })
        field_sizeC.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                size_C = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }
                calculate(rootView)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sizeH.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeH = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }
                calculate(rootView)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_cost.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            @RequiresApi(Build.VERSION_CODES.N)
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_cost = Integer.parseInt(s.toString())
                calculate(rootView)

            }

            override fun afterTextChanged(s: Editable?) {}
        })

        return rootView
    }

    @SuppressLint("SetTextI18n")
    fun calculate(rootView: View){
        if (size_A != null && size_B != null && size_C != null){
            val size_a = size_A!! * unit_for_first_size
            val size_b = size_B!! * unit_for_second_size
            val size_c = size_C!! * unit_for_third_size

            val S = size_a * ((size_b + size_c) / 2)
            val P = size_a*2 + size_b + size_c

            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING

            var string = "Площадь фигуры: ${df.format(S)} м"
            //string += "\nПериметр фигуры: ${df.format(P)} м"


            if (number_sizeH != null){
                val h = number_sizeH!! * unit_for_forth_size
                val V = S * h
                //val S_side_surface = P * h
                val weight = V * 2400 // 2400 - средний вес тяжёлого бетона
                val soil_load = weight / (S * 10000)

                string += "\n\n"
                string += "\nОбъём плиты: ${df.format(V)} м3"
                //string += "\nПлощадь боковой поверхности: ${df.format(S_side_surface)} м"
                string += "\nВес бетона: ${weight.toInt()} кг"
                string += "\nНагрузка на почву: ${df.format(soil_load)} кг/см2"


                if (number_cost != null){
                    val cost = V * number_cost!!
                    string += "\nСтоимость: ${cost.toInt()} руб"
                }
            }
            val output_window = rootView.findViewById<TextView>(R.id.output_result_1)
            if (string.isNotEmpty()) { output_window.setText(string) }
        }
    }
}