package com.example.dip5.thirdFragment.fragments

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.dip5.R
import com.google.android.play.core.splitinstall.d
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.math.PI

class CulcCyrcleFragment : Fragment() {

    var diametr_D: Float? = null
    var number_sizeH: Float? = null
    var number_cost:  Int? = null


    var unit_for_first_size:  Float = 1.0F
    var unit_for_second_size: Float = 1.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val list = listOf("м", "см", "мм")

        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_third_culc_cyrcle, container, false)

        //===============================================================================================================
        // actionBar configuration
        (activity as AppCompatActivity).supportActionBar?.title = "Вычисление площади круга"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //====================================================================================================================
        // initialisation basic components
        val spinner_dianetrA = rootView.findViewById<Spinner>(R.id.spinner_diametrA)
        val spinner_sizeH = rootView.findViewById<Spinner>(R.id.spinner_sizeH)

        val spinner_adapter = ArrayAdapter(rootView.context, android.R.layout.simple_spinner_item, list)
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner_dianetrA.adapter = spinner_adapter
        spinner_sizeH.adapter = spinner_adapter

        val field_diametrA = rootView.findViewById<EditText>(R.id.editText_diametrD)
        val field_sizeH = rootView.findViewById<EditText>(R.id.editText_sizeH)
        val field_cost = rootView.findViewById<EditText>(R.id.editText_cost)

        //======================================================================================================================
        //implementation listeners for Spinners
        spinner_dianetrA.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        unit_for_first_size = 1.0F
                    }
                    1 -> {
                        unit_for_first_size = 0.01F
                    }
                    2 -> {
                        unit_for_first_size = 0.001F
                    }
                }
                calculate(rootView)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinner_sizeH.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_second_size = position
                when(position){
                    0 -> {
                        unit_for_second_size = 1.0F
                    }
                    1 -> {
                        unit_for_second_size = 0.01F
                    }
                    2 -> {
                        unit_for_second_size = 0.001F
                    }
                }
                calculate(rootView)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        //=====================================================================================================
        // implementation listeners for fields
        field_diametrA.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                diametr_D = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }
                calculate(rootView)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sizeH.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeH = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }
                calculate(rootView)
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_cost.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            @RequiresApi(Build.VERSION_CODES.N)
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_cost = Integer.parseInt(s.toString())
                calculate(rootView)

            }

            override fun afterTextChanged(s: Editable?) {}
        })

        return rootView
    }

    @SuppressLint("SetTextI18n")
    fun calculate(rootView: View){
        if (diametr_D != null){
            val radius = (diametr_D!! * unit_for_first_size) / 2
            val S = PI * radius * radius
            val P = 2 * PI * radius

            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING

            var string = "Площадь фигуры: ${df.format(S)} м"
                string += "\nПериметр фигуры: ${df.format(P)} м"



            if (number_sizeH != null){
                val h = number_sizeH!!

                val V = S * h
                val S_side_surface = P * h
                val weight = V * 2400 // 2400 - средний вес тяжёлого бетона
                val soil_load = weight / (S * 10000)

                string += "\n\n"
                string += "\nОбъём плиты: ${df.format(V)} м3"
                string += "\nПлощадь боковой поверхности: ${df.format(S_side_surface)} м"
                string += "\nВес бетона: ${weight.toInt()} кг"
                string += "\nНагрузка на почву: ${df.format(soil_load)} кг/см2"


                if (number_cost != null){
                    val cost = V * number_cost!!
                    string += "\nСтоимость: ${cost.toInt()} руб"
                }
            }

            val output_window = rootView.findViewById<TextView>(R.id.output_result_1)
            if (string.isNotEmpty()) { output_window.setText(string) }

        }

    }

}