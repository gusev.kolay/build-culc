package com.example.dip5.thirdFragment

data class ListCardItem(
    var destination: Int,
    var icon: Int
)
