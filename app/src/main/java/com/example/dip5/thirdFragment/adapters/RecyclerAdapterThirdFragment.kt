package com.example.dip5.thirdFragment.adapters
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.dip5.R
import com.example.dip5.thirdFragment.ListCardItem

class RecyclerAdapterThirdFragment(listItem: ArrayList<ListCardItem>) : RecyclerView.Adapter<RecyclerAdapterThirdFragment.ViewHolder>(){

    val lisrArr = listItem

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{

        var button: ListCardItem? = null

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(item: ListCardItem){
            button = item
            itemView.findViewById<ImageView>(R.id.iv_image_card).setImageResource(button!!.icon)
            //itemView.findViewById<TextView>(R.id.tv_title).text = button?.name
        }
        override fun onClick(v: View?) {
            v?.findNavController()?.navigate(button!!.destination)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.fragment_third_item_recycler, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lisrArr[position])
    }

    override fun getItemCount(): Int {
        return lisrArr.size
    }
}