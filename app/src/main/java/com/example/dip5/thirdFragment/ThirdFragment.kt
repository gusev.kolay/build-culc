package com.example.dip5.thirdFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dip5.R
import com.example.dip5.firstFragment.innerFragment.FoundationsAndBeton.ListButtonItem
import com.example.dip5.firstFragment.innerFragment.FoundationsAndBeton.Rec_adp_for_FoundAndBeton
import com.example.dip5.thirdFragment.adapters.RecyclerAdapterThirdFragment

class ThirdFragment : Fragment() {

    var adapter : RecyclerAdapterThirdFragment? = null

    private val list_button =  ArrayList<ListCardItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        list_button.addAll(
            listOf((ListCardItem(R.id.action_mainFragment_to_culcCyrcleFragment, R.drawable.ic_frag3_cyrcle)),
                (ListCardItem(R.id.action_mainFragment_to_culcSquareFragment, R.drawable.ic_frag3_square)),
                (ListCardItem(R.id.action_mainFragment_to_culcRectangleFragment, R.drawable.ic_frag3_rectangle)),
                (ListCardItem(R.id.action_mainFragment_to_culcParallelogramFragment, R.drawable.ic_frag3_parallelogram)),
                (ListCardItem(R.id.action_mainFragment_to_culcTriangleFragment, R.drawable.ic_frag3_triangle)),
                (ListCardItem(R.id.action_mainFragment_to_culcTrapezeFragment, R.drawable.ic_frag3_trapeze)),
                (ListCardItem(R.id.action_mainFragment_to_culcFigure1Fragment, R.drawable.ic_frag3_figure_1)),
                (ListCardItem(R.id.action_mainFragment_to_culcFigure2Fragment, R.drawable.ic_frag3_figure_2)),
                (ListCardItem(R.id.action_mainFragment_to_culcFigure3Fragment, R.drawable.ic_frag3_figure_3)),
                (ListCardItem(R.id.action_mainFragment_to_culcFigure4ragment, R.drawable.ic_frag3_figure_4)),
                (ListCardItem(R.id.action_mainFragment_to_culcFigure5Fragment, R.drawable.ic_frag3_figure_5)),
                (ListCardItem(R.id.action_mainFragment_to_culcFigure6Fragment, R.drawable.ic_frag3_figure_6)),
                (ListCardItem(R.id.action_mainFragment_to_culcFigure7Fragment, R.drawable.ic_frag3_figure_7)),
                (ListCardItem(R.id.action_mainFragment_to_culcFigure8Fragment, R.drawable.ic_frag3_figure_8))
            )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView: View =  inflater.inflate(R.layout.fragment_third, container, false)
        val recView: RecyclerView = rootView.findViewById(R.id.rcView_third_fragment)


        recView.layoutManager = GridLayoutManager(rootView.context, 3)
        adapter =  RecyclerAdapterThirdFragment(list_button)
        recView.adapter = adapter

        return  rootView
    }
}