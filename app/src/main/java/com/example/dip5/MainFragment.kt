package com.example.dip5

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.dip5.adapters.ViewPagerAdapter
import com.example.dip5.firstFragment.FirstFragment
import com.example.dip5.secondFragment.SecondFragment
import com.example.dip5.thirdFragment.ThirdFragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view =  inflater.inflate(R.layout.fragment_main, container, false)
        val viewPager2 = view.findViewById<ViewPager2>(R.id.viewPager)
        val tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)

        val fragmentList = arrayListOf<Fragment>(
            FirstFragment(),
            SecondFragment(),
            ThirdFragment()
        )


        //val adapter = ViewPagerAdapter(fragmentList, childFragmentManager, lifecycle)

        val adapter = ViewPagerAdapter(
            fragmentList,
            requireActivity().supportFragmentManager,
            lifecycle)

        viewPager2.adapter = adapter

        TabLayoutMediator(tabLayout, viewPager2)
        {tab, position->
            when(position){
                0 -> {
                    //tab.text = "str1"
                    tab.setIcon(R.drawable.ic_home)
                }
                1 -> {
                    //tab.text = "str1"
                    tab.setIcon(R.drawable.ic_hold_records)
                }
                2 -> {
                    //tab.text = "str1"
                    tab.setIcon(R.drawable.ic_notes)
                }
            }
        }.attach()

        return view
    }
}