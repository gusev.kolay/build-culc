package com.example.dip5.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.dip5.firstFragment.FirstFragment
import com.example.dip5.secondFragment.SecondFragment
import com.example.dip5.thirdFragment.ThirdFragment

class ViewPagerAdapter(
        list: ArrayList<Fragment>,
        fragmentManager: FragmentManager,
        lifecycle: Lifecycle
): FragmentStateAdapter(fragmentManager, lifecycle) {

    private val listFragment: ArrayList<Fragment> = list

    override fun getItemCount(): Int {
        return listFragment.size
    }

    override fun createFragment(position: Int): Fragment {
        return listFragment[position]
    }

}