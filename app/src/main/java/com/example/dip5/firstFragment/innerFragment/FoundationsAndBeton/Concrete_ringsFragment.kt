package com.example.dip5.firstFragment.innerFragment.FoundationsAndBeton

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.dip5.R
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.math.PI
import kotlin.math.pow

class Concrete_ringsFragment : Fragment() {

    var number_sizeT: Float? = null
    var number_sizeH: Float? = null
    var number_sizeD: Float? = null
    var number_cost: Int? = null

    var unit_for_first_size: Float = 1.0F
    var unit_for_second_size: Float = 1.0F
    var unit_for_third_size: Float = 1.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val list = listOf("м", "см", "мм")

        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_first_foundations_concrete_rings, container, false)

        //===============================================================================================================
        // actionBar configuration
        (activity as AppCompatActivity).supportActionBar?.title = "Расчёт бетонных колец"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //====================================================================================================================
        // initialisation basic components
        val spinner_sizeH = rootView.findViewById<Spinner>(R.id.spinner_sizeH)
        val spinner_sizeT = rootView.findViewById<Spinner>(R.id.spinner_sizeT)
        val spinner_sizeD = rootView.findViewById<Spinner>(R.id.spinner_sizeD)

        val spinner_adapter = ArrayAdapter(rootView.context, android.R.layout.simple_spinner_item, list)
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner_sizeH.adapter = spinner_adapter
        spinner_sizeT.adapter = spinner_adapter
        spinner_sizeT.setSelection(1)
        spinner_sizeD.adapter = spinner_adapter

        val field_sizeH = rootView.findViewById<EditText>(R.id.editText_sizeH)
        val field_sizeT = rootView.findViewById<EditText>(R.id.editText_sizeT)
        val field_sizeD = rootView.findViewById<EditText>(R.id.editText_sizeD)
        val field_cost = rootView.findViewById<EditText>(R.id.editText_cost)

        //======================================================================================================================
        //implementation listeners for Spinners
        spinner_sizeT.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        unit_for_first_size = 1.0F
                    }
                    1 -> {
                        unit_for_first_size = 0.01F
                    }
                    2 -> {
                        unit_for_first_size = 0.001F
                    }
                }
                if (isNotNullable()) {
                    calculate(number_sizeT!! * unit_for_first_size,
                            number_sizeH!! * unit_for_second_size,
                            number_sizeD!! * unit_for_third_size,
                            rootView)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        spinner_sizeH.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_second_size = position
                when (position) {
                    0 -> {
                        unit_for_second_size = 1.0F
                    }
                    1 -> {
                        unit_for_second_size = 0.01F
                    }
                    2 -> {
                        unit_for_second_size = 0.001F
                    }
                }
                if (isNotNullable()) {
                    calculate(number_sizeT!! * unit_for_first_size,
                            number_sizeH!! * unit_for_second_size,
                            number_sizeD!! * unit_for_third_size,
                            rootView)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        spinner_sizeD.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_third_size = position
                when (position) {
                    0 -> {
                        unit_for_third_size = 1.0F
                    }
                    1 -> {
                        unit_for_third_size = 0.01F
                    }
                    2 -> {
                        unit_for_third_size = 0.001F
                    }
                }
                if (isNotNullable()) {
                    calculate(number_sizeT!! * unit_for_first_size,
                            number_sizeH!! * unit_for_second_size,
                            number_sizeD!! * unit_for_third_size,
                            rootView)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        //=====================================================================================================
        // implementation listeners for fields
        field_sizeH.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeH = if (s.isNullOrEmpty()) {
                    null
                } else {
                    (s.toString()).toFloat()
                }

                if (isNotNullable()) {
                    calculate(number_sizeT!! * unit_for_first_size,
                            number_sizeH!! * unit_for_second_size,
                            number_sizeD!! * unit_for_third_size,
                            rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sizeT.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeT = if (s.isNullOrEmpty()) {
                    null
                } else {
                    (s.toString()).toFloat()
                }

                if (isNotNullable()) {
                    calculate(number_sizeT!! * unit_for_first_size,
                            number_sizeH!! * unit_for_second_size,
                            number_sizeD!! * unit_for_third_size,
                            rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })


        field_sizeD.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeD = if (s.isNullOrEmpty()) {
                    null
                } else {
                    (s.toString()).toFloat()
                }

                if (isNotNullable()) {
                    calculate(number_sizeT!! * unit_for_first_size,
                            number_sizeH!! * unit_for_second_size,
                            number_sizeD!! * unit_for_third_size,
                            rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })


        field_cost.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            @RequiresApi(Build.VERSION_CODES.N)
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_cost = Integer.parseInt(s.toString())
                if (isNotNullable()) {
                    calculate(number_sizeT!! * unit_for_first_size,
                            number_sizeH!! * unit_for_second_size,
                            number_sizeD!! * unit_for_third_size,
                            rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        return rootView
    }

    @SuppressLint("SetTextI18n")
    fun calculate(t_: Float?, h_: Float?, d_: Float?, rootView: View) {
        var t: Float = t_!!.toFloat()
        var h: Float = h_!!.toFloat()
        var d: Float = d_!!.toFloat()

        Log.d("S2", "$ t = ${t} h = ${h} d = ${d}")
        Log.d("coef", "$ unit_for_first_size = ${unit_for_first_size} unit_for_second_size = ${unit_for_second_size} unit_for_third_size = ${unit_for_third_size}")

        var string = ""

        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING

        val S2 = PI * ((d - 2*t) / 2).pow(2)
        //Log.d("S2", "${S2}")
        val S = PI * (d / 2).pow(2) - S2

        val V = S * h
        val V_inner = S2 * h
        val weight = V * 2400 // 2400 - средний вес тяжёлого бетона
        val soil_load = weight / (S * 10000)



        string += "Площадь основания: ${df.format(S)} м"
        string += "\nОбъём бетона: ${df.format(V)} м3"
        string += "\nВнутренний объём: ${df.format(V_inner)} м"
        string += "\nВес бетона: ${weight.toInt()} кг"
        string += "\nНагрузка на почву: ${df.format(soil_load)} кг/см2"



        if (number_cost != null) {
            val cost = V * number_cost!!
            string += "\nСтоимость: ${cost.toInt()} руб"
        }

        val output_window = rootView.findViewById<TextView>(R.id.output_result)
        if (string.isNotEmpty()) {
            output_window.setText(string)
        }


    }

    fun isNotNullable(): Boolean {
        var par = false

        if (number_sizeT != null &&
                number_sizeH != null &&
                number_sizeT != null &&
                number_sizeD != null) {

            par = true
        }
        return par
    }

}