package com.example.dip5.firstFragment.innerFragment.FoundationsAndBeton

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.dip5.R
import java.math.RoundingMode
import java.text.DecimalFormat

class Tape_base_3_Fragment : Fragment() {

    var number_sizeA: Float? = null
    var number_sizeB: Float? = null
    var number_sizeC: Float? = null
    var number_sizeD: Float? = null
    var number_sizeE: Float? = null
    var number_cost:  Int? = null

    var unit_for_first_size:  Float = 1.0F
    var unit_for_second_size: Float = 1.0F
    var unit_for_third_size:  Float = 1.0F
    var unit_for_fourth_size:  Float = 1.0F
    var unit_for_fifth_size:  Float = 1.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val list = listOf("м", "см", "мм")

        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_first_foundations_tape_base_3, container, false)

        //===============================================================================================================
        // actionBar configuration
        (activity as AppCompatActivity).supportActionBar?.title = "Расчёт ленточного фундамента"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //====================================================================================================================
        // initialisation basic components
        val spinner_sizeA = rootView.findViewById<Spinner>(R.id.spinner_sizeA)
        val spinner_sizeB = rootView.findViewById<Spinner>(R.id.spinner_sizeB)
        val spinner_sizeC = rootView.findViewById<Spinner>(R.id.spinner_sizeC)
        val spinner_sizeD = rootView.findViewById<Spinner>(R.id.spinner_sizeD)
        val spinner_sizeE = rootView.findViewById<Spinner>(R.id.spinner_sizeE)

        val spinner_adapter = ArrayAdapter(rootView.context, android.R.layout.simple_spinner_item, list)
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner_sizeA.adapter = spinner_adapter
        spinner_sizeB.adapter = spinner_adapter
        spinner_sizeC.adapter = spinner_adapter
        spinner_sizeD.adapter = spinner_adapter
        spinner_sizeE.adapter = spinner_adapter

        val field_sizeA = rootView.findViewById<EditText>(R.id.editText_sizeA)
        val field_sizeB = rootView.findViewById<EditText>(R.id.editText_sizeB)
        val field_sizeC = rootView.findViewById<EditText>(R.id.editText_sizeC)
        val field_sizeD = rootView.findViewById<EditText>(R.id.editText_sizeD)
        val field_sizeE = rootView.findViewById<EditText>(R.id.editText_sizeE)
        val field_cost = rootView.findViewById<EditText>(R.id.editText_cost)

        //======================================================================================================================
        //implementation listeners for Spinners
        spinner_sizeA.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position){
                    0 -> {
                        unit_for_first_size = 1.0F
                    }
                    1 -> {
                        unit_for_first_size = 0.01F
                    }
                    2 -> {
                        unit_for_first_size = 0.001F
                    }
                }
                if (isNotNullable()){
                    calculate(number_sizeA!! * unit_for_first_size,
                        number_sizeB!! * unit_for_second_size,
                        number_sizeC!! * unit_for_third_size,
                        number_sizeD!! * unit_for_fourth_size,
                        number_sizeE!! * unit_for_fifth_size,
                        rootView) }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        spinner_sizeB.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_second_size = position
                when(position){
                    0 -> {
                        unit_for_second_size = 1.0F
                    }
                    1 -> {
                        unit_for_second_size = 0.01F
                    }
                    2 -> {
                        unit_for_second_size = 0.001F
                    }
                }
                if (isNotNullable()){
                    calculate(number_sizeA!! * unit_for_first_size,
                        number_sizeB!! * unit_for_second_size,
                        number_sizeC!! * unit_for_third_size,
                        number_sizeD!! * unit_for_fourth_size,
                        number_sizeE!! * unit_for_fifth_size,
                        rootView) }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        spinner_sizeC.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_third_size = position
                when(position){
                    0 -> {
                        unit_for_third_size = 1.0F
                    }
                    1 -> {
                        unit_for_third_size = 0.01F
                    }
                    2 -> {
                        unit_for_third_size = 0.001F
                    }
                }
                if (isNotNullable()){
                    calculate(number_sizeA!! * unit_for_first_size,
                        number_sizeB!! * unit_for_second_size,
                        number_sizeC!! * unit_for_third_size,
                        number_sizeD!! * unit_for_fourth_size,
                        number_sizeE!! * unit_for_fifth_size,
                        rootView) }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinner_sizeD.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_third_size = position
                when(position){
                    0 -> {
                        unit_for_fourth_size = 1.0F
                    }
                    1 -> {
                        unit_for_fourth_size = 0.01F
                    }
                    2 -> {
                        unit_for_fourth_size = 0.001F
                    }
                }
                if (isNotNullable()){
                    calculate(number_sizeA!! * unit_for_first_size,
                        number_sizeB!! * unit_for_second_size,
                        number_sizeC!! * unit_for_third_size,
                        number_sizeD!! * unit_for_fourth_size,
                        number_sizeE!! * unit_for_fifth_size,
                        rootView)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinner_sizeE.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_third_size = position
                when(position){
                    0 -> {
                        unit_for_fifth_size = 1.0F
                    }
                    1 -> {
                        unit_for_fifth_size = 0.01F
                    }
                    2 -> {
                        unit_for_fifth_size = 0.001F
                    }
                }
                if (isNotNullable()){
                    calculate(number_sizeA!! * unit_for_first_size,
                        number_sizeB!! * unit_for_second_size,
                        number_sizeC!! * unit_for_third_size,
                        number_sizeD!! * unit_for_fourth_size,
                        number_sizeE!! * unit_for_fifth_size,
                        rootView)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }



        //=====================================================================================================
        // implementation listeners for fields
        field_sizeA.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeA = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }

                if (isNotNullable()){
                    calculate(number_sizeA, number_sizeB, number_sizeC, number_sizeD, number_sizeE, rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sizeB.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeB = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }

                if (isNotNullable()){
                    calculate(number_sizeA, number_sizeB, number_sizeC, number_sizeD, number_sizeE, rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sizeC.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeC = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }

                if (isNotNullable()){
                    calculate(number_sizeA, number_sizeB, number_sizeC, number_sizeD, number_sizeE, rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sizeD.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeD = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }

                if (isNotNullable()){
                    calculate(number_sizeA, number_sizeB, number_sizeC, number_sizeD, number_sizeE, rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sizeE.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_sizeE = if (s.isNullOrEmpty()){
                    null
                } else{
                    (s.toString()).toFloat()
                }

                if (isNotNullable()){
                    calculate(number_sizeA, number_sizeB, number_sizeC, number_sizeD, number_sizeE, rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_cost.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            @RequiresApi(Build.VERSION_CODES.N)
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                number_cost = Integer.parseInt(s.toString())
                if (isNotNullable()){
                    calculate(number_sizeA, number_sizeB, number_sizeC, number_sizeD, number_sizeE, rootView)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        return rootView
    }

    @SuppressLint("SetTextI18n")
    fun calculate(a_: Float?, b_: Float?, c_: Float?, d_: Float?, e_: Float?, rootView: View){
        var a: Float = a_!!.toFloat()
        var b: Float = b_!!.toFloat()
        var c: Float = c_!!.toFloat()
        var d: Float = d_!!.toFloat()
        var e: Float = e_!!.toFloat()

        var string = ""

        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING

        val t = (a - 2*d) * (b - 3*d - e)
        val r = (a - 3*d) * e
        val S = a * b - (t + r)

        val V = S * c
        val P = 2 * a + 2 * b
        val S_side_surface = 2 * c*a + 2 * c*b
        val weight = V * 2400 // 2400 - средний вес тяжёлого бетона
        val soil_load = weight / (S * 10000)



        string += "Площадь основания: ${df.format(S)} м"
        string += "\nОбъём ленты: ${df.format(V)} м3"
        string += "\nНаружный периметр ленты: $P м"
        string += "\nПлощадь боковой поверхности: $S_side_surface м"
        string += "\nВес бетона: ${weight.toInt()} кг"
        string += "\nНагрузка на почву: ${df.format(soil_load)} кг/см2"



        if (number_cost != null){
            val cost = V * number_cost!!
            string += "\nСтоимость: ${cost.toInt()} руб"
        }

        val output_window = rootView.findViewById<TextView>(R.id.output_result)
        if (string.isNotEmpty()) { output_window.setText(string) }


    }

    fun isNotNullable(): Boolean{
        var par = false

        if (number_sizeA != null &&
            number_sizeB != null &&
            number_sizeC != null &&
            number_sizeD != null &&
            number_sizeE != null){

                par = true
        }
        return par
    }

}