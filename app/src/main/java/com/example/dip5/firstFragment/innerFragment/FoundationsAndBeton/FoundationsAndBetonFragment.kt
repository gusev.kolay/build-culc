package com.example.dip5.firstFragment.innerFragment.FoundationsAndBeton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dip5.R


class FoundationsAndBetonFragment : Fragment() {

    var adapter : Rec_adp_for_FoundAndBeton? = null

    private val list_button =  ArrayList<ListButtonItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        list_button.addAll(
            listOf((ListButtonItem("Расчёт фундаментной плиты", R.id.action_foundationsAndBetonFragment_to_calculationFoundationPlateFragment)),
                (ListButtonItem("Расчёт ленточного фундамента", R.id.action_foundationsAndBetonFragment_to_tape_baseFragment)),
                (ListButtonItem("Расчёт ленточного фундамента с двумя отделами", R.id.action_foundationsAndBetonFragment_to_tape_base_2_Fragment)),
                (ListButtonItem("Расчёт ленточного фундамента с тремя отделами", R.id.action_foundationsAndBetonFragment_to_tape_base_3_Fragment)),
                (ListButtonItem("Расчёт ленточного фундамента с четырьмя отделами", R.id.action_foundationsAndBetonFragment_to_tape_base_4_Fragment)),
                (ListButtonItem("Расчёт состава бетона", R.id.action_foundationsAndBetonFragment_to_concrete_compositionFragment)),
                (ListButtonItem("Расчёт материла для бетонных колец", R.id.action_foundationsAndBetonFragment_to_concrete_ringsFragment))
               )
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView: View =  inflater.inflate(R.layout.fragment_first_foundations_and_beton, container, false)
        val recView: RecyclerView = rootView.findViewById(R.id.rcView_1_found_and_beton)



        recView.layoutManager = LinearLayoutManager(rootView.context)
        adapter = Rec_adp_for_FoundAndBeton(list_button)
        recView.adapter = adapter

        return  rootView
    }

}