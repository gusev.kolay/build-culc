package com.example.dip5.firstFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dip5.R
import com.example.dip5.firstFragment.adapters.RecyclerAdapterFirstFragment


class FirstFragment : Fragment() {
    var adapter : RecyclerAdapterFirstFragment? = null

    private val list_button =  ArrayList<ListButtonItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView: View = inflater.inflate(R.layout.fragment_first, container, false)
        val rcView : RecyclerView = rootView.findViewById(R.id.rcView_1)


        list_button.addAll(
                listOf(ListButtonItem("Фундаменты и бетон", R.id.action_mainFragment_to_foundationsAndBeton, "Плита, Ленточный фундамент, Бетон"),
                        ListButtonItem("Стены", R.id.action_mainFragment_to_wallsFragment),
                        ListButtonItem("Пиломатериалы", R.id.action_mainFragment_to_lumberFragment),
                        ListButtonItem("Отделка и материалы", R.id.action_mainFragment_to_finishingMaterialsFragment),
                        ListButtonItem("Земляные работы", R.id.action_mainFragment_to_earthWorkFragment),
                        ListButtonItem("Кровли", R.id.action_mainFragment_to_roofsFragment))
        )

        rcView.layoutManager = LinearLayoutManager(rootView.context)

        adapter = RecyclerAdapterFirstFragment(list_button)
        rcView.adapter = adapter

        return rootView
    }
}