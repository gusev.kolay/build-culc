package com.example.dip5.firstFragment.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.dip5.R
import com.example.dip5.firstFragment.ListButtonItem

class RecyclerAdapterFirstFragment(listArray:ArrayList<ListButtonItem>) : RecyclerView.Adapter<RecyclerAdapterFirstFragment.ViewHolder>(){

    var listArrR = listArray


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{
        private var button: ListButtonItem? = null

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(item: ListButtonItem) {
            button = item
            itemView.findViewById<TextView>(R.id.tv_title).text = item.name
            itemView.findViewById<TextView>(R.id.tv_description).text = item.description
        }

        override fun onClick(v: View?) {
            v?.findNavController()?.navigate(button!!.destination)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.content_fragment_1, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listArrR[position])

    }

    override fun getItemCount(): Int {
        return listArrR.size
    }
}