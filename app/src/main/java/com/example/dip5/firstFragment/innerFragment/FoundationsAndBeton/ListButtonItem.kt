package com.example.dip5.firstFragment.innerFragment.FoundationsAndBeton

data class ListButtonItem(
        var name: String,
        var destination: Int
)
