package com.example.dip5.firstFragment

data class ListButtonItem(
    var name: String,
    var destination: Int,
    var description: String = "",
    var icon: Int? = null
)
