package com.example.dip5.firstFragment.innerFragment.FoundationsAndBeton

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.Toast.LENGTH_SHORT
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.dip5.R
import com.google.android.play.core.splitinstall.d
import java.math.MathContext
import java.math.RoundingMode
import java.text.DecimalFormat

class Concrete_compositionFragment : Fragment() {

    private lateinit var spinner_concrete_mark: Spinner
    private lateinit var spinner_cement_mark: Spinner
    private lateinit var spinner_mobility_concrete: Spinner
    private lateinit var spinner_plastifier_reducion: Spinner

    private lateinit var field_concrete_volume: EditText
    private lateinit var field_plastifier_part: EditText
    private lateinit var field_sand_humidity: EditText
    private lateinit var field_gravel_humidity: EditText

    var concrete_volume: Float? = null // объём бетона

    var concrete_mark: Int? = null // марка бетона
    var cement_mark: Int? = null // марка цемента
    var plastifier_reducion: Int? = null // редуцирующий эффект пластификатора

    var plastifier_part: Float? = null // массовая доля пластификатора в бетоне
    var sand_humidity: Float? = null // влажность песка
    var gravel_humidity: Float? = null // влажность бетона


    var water_volume: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val list_concrete_mark = listOf(
                "М-100 = В 7.5",
                "М-150 = В 10",
                "М-200 = В 15",
                "М-250 = В 20",
                "М-300 = В 25")
        val list_cement_mark = listOf(
                "М-400",
                "М-500")
        val list_mobility_concrete = listOf(
                "П-1",
                "П-2",
                "П-3",
                "П-4")
        val list_plastifier_reducion = listOf(
                "10%",
                "20%",
                "30%")

        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_first_foundations_concrete_composition, container, false)

        //===============================================================================================================
        // Implemetation onClick
        val result = rootView.findViewById<ImageView>(R.id.image_calculate)
        result.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                calculate(rootView)
            }
        })
        val reset = rootView.findViewById<ImageView>(R.id.image_reset)
        reset.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                init(rootView)
                Log.d("tut", "tut")
            }
        })

        //===============================================================================================================
        // actionBar configuration
        (activity as AppCompatActivity).supportActionBar?.title = "Расчёт бетона"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //====================================================================================================================

        spinner_concrete_mark = rootView.findViewById<Spinner>(R.id.spinner_concrete_mark)
        spinner_cement_mark = rootView.findViewById<Spinner>(R.id.spinner_cement_mark)
        spinner_mobility_concrete = rootView.findViewById<Spinner>(R.id.spinner_mobility_concrete)
        spinner_plastifier_reducion = rootView.findViewById<Spinner>(R.id.spinner_plastifier_reducion)

        //=================================================================================================================================
        // Create and initialisation spinners adapters
        val spinner_adapter_concrete_mark = ArrayAdapter(rootView.context, android.R.layout.simple_spinner_item, list_concrete_mark)
        spinner_adapter_concrete_mark.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val spinner_adapter_cement_mark = ArrayAdapter(rootView.context, android.R.layout.simple_spinner_item, list_cement_mark)
        spinner_adapter_concrete_mark.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val spinner_adapter_mobility_concrete = ArrayAdapter(rootView.context, android.R.layout.simple_spinner_item, list_mobility_concrete)
        spinner_adapter_concrete_mark.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val spinner_adapter_plastifier_reducion = ArrayAdapter(rootView.context, android.R.layout.simple_spinner_item, list_plastifier_reducion)
        spinner_adapter_concrete_mark.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        //=================================================================================================================================
        // Instance spinners adapters
        spinner_concrete_mark.adapter = spinner_adapter_concrete_mark
        spinner_cement_mark.adapter = spinner_adapter_cement_mark
        spinner_mobility_concrete.adapter = spinner_adapter_mobility_concrete
        spinner_plastifier_reducion.adapter = spinner_adapter_plastifier_reducion

        field_concrete_volume = rootView.findViewById<EditText>(R.id.editText_concrete_volume)
        field_plastifier_part = rootView.findViewById<EditText>(R.id.editText_plastifier_part)
        field_sand_humidity = rootView.findViewById<EditText>(R.id.editText_sand_humidity)
        field_gravel_humidity = rootView.findViewById<EditText>(R.id.editText_gravel_humidity)

        //======================================================================================================================
        //initialization members this fragment
        init(rootView)

        //======================================================================================================================
        //implementation listeners for Spinners
        spinner_concrete_mark.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        concrete_mark = 100
                    }
                    1 -> {
                        concrete_mark = 150
                    }
                    2 -> {
                        concrete_mark = 200
                    }
                    3 -> {
                        concrete_mark = 250
                    }
                    4 -> {
                        concrete_mark = 300
                    }
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinner_cement_mark.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        cement_mark = 400
                    }
                    1 -> {
                        cement_mark = 500
                    }
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinner_mobility_concrete.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        water_volume = 180
                    }
                    1 -> {
                        water_volume = 195
                    }
                    2 -> {
                        water_volume = 210
                    }
                    3 -> {
                        water_volume = 230
                    }
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        spinner_plastifier_reducion.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //unit_for_third_size = position
                when (position) {
                    0 -> {
                        plastifier_reducion = 10
                    }
                    1 -> {
                        plastifier_reducion = 20
                    }
                    2 -> {
                        plastifier_reducion = 30
                    }
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }


        //=====================================================================================================
        // implementation listeners for fields
        field_concrete_volume.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                concrete_volume = if (s.isNullOrEmpty()) {
                    null
                } else {
                    (s.toString()).toFloat()
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_sand_humidity.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                sand_humidity = if (s.isNullOrEmpty()) {
                    null
                } else {
                    (s.toString()).toFloat()
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_gravel_humidity.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                gravel_humidity = if (s.isNullOrEmpty()) {
                    null
                } else {
                    (s.toString()).toFloat()
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        field_plastifier_part.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                plastifier_part = if (s.isNullOrEmpty()) {
                    null
                } else {
                    (s.toString()).toFloat()
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })


        return rootView
    }


    @SuppressLint("SetTextI18n")
    fun calculate(rootView: View){
        val result: Float = concrete_volume!!
        var water: Float = water_volume!!.toFloat()

        var plastifier: Float? = null
        val water_cement: Float = ((0.6F * cement_mark!!) / (concrete_mark!! + 0.5F * 0.6F * cement_mark!!)).toFloat()
        var cement: Float = water / water_cement



        if (plastifier_part != null){
            val plastifier_red: Float = plastifier_reducion!! * 0.01F
            water -= (water * plastifier_red).toInt()
            cement = water / water_cement

            plastifier = (cement * (1 + plastifier_part!! * 0.01) - cement).toFloat()
        }

        var gravel = 1300.0F
        var sand = 2400.0F - water - cement - gravel
        val humidity1 = ((1 + sand_humidity!! * 0.01) * sand - sand).toFloat()
        sand += humidity1
        val humidity2 = ((1 + gravel_humidity!! * 0.01) * gravel - gravel).toFloat()
        gravel += humidity2.toInt()

        water = water - (humidity1 + humidity2)

        var string = ""

        val df = DecimalFormat("#.#")
        df.roundingMode = RoundingMode.CEILING

        if (isNotNullable()){
            string += "Кол-во воды: ${df.format(water * result)} л"
            string += "\nКол-во щебня: ${df.format(gravel * result)} кг"
            string += "\nКол-во цемента: ${df.format(cement * result)} кг"
            string += "\nКол-во песка: ${df.format(sand * result)} кг"
            if (plastifier_part != null){
                string += "\nКол-во пластификатора: ${df.format(plastifier!! * result)} кг"
            }
        }
        else{
            Toast.makeText(rootView.context, "Заполните пустые поля формы!", LENGTH_SHORT).show()
        }

        val output_window = rootView.findViewById<TextView>(R.id.output_result)
        if (string.isNotEmpty()) { output_window.setText(string) }
    }

    fun isNotNullable(): Boolean{
        var par = false

        if (concrete_volume != null &&
            sand_humidity != null &&
            gravel_humidity != null){
            par = true
        }
        return par
    }

    fun init(context: View){
        val string = "Claered!"
        val output_window = context.findViewById<TextView>(R.id.output_result)
        if (string.isNotEmpty()) { output_window.setText(string) }

        spinner_concrete_mark.setSelection(0)
        spinner_cement_mark.setSelection(0)
        spinner_mobility_concrete.setSelection(0)
        spinner_plastifier_reducion.setSelection(0)

        if (field_plastifier_part.text == null){
            plastifier_part = field_plastifier_part.text.toString().toFloat()
        }

        sand_humidity = field_sand_humidity.text.toString().toFloat()
        gravel_humidity = field_gravel_humidity.text.toString().toFloat()
    }
}