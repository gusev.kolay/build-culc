package com.example.dip5.firstFragment.innerFragment.FoundationsAndBeton

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.dip5.R

class Rec_adp_for_FoundAndBeton(listItem: ArrayList<ListButtonItem>) : RecyclerView.Adapter<Rec_adp_for_FoundAndBeton.ViewHolder>() {

    val lisrArr = listItem

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{

        var button: ListButtonItem? = null

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(item: ListButtonItem){
            button = item
            itemView.findViewById<TextView>(R.id.tv_title).text = button?.name
        }
        override fun onClick(v: View?) {
            v?.findNavController()?.navigate(button!!.destination)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.content_fragment_1_foundation_and_beton, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lisrArr[position])
    }

    override fun getItemCount(): Int {
        return lisrArr.size
    }
}